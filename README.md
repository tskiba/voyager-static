# Static Voyager

## Usage

### Install as local tool

`This tool requires node to work`

If you do not yet have a manifest create on in the root directory of your project

```shell
dotnet new tool-manifest
```

Install the tool

```shell
dotnet tool install Static.Voyager
```

Run it

```shell
dotnet tool run graph-to-voyager -- ./path/to/schema.graphql
```

## Development

### Test Locally

```shell
dotnet run -- my-relative-schema-path
```

### Create Package

```shell
dotnet pack
```

### As tool

```shell
graph-to-voyager -- my-relative-schema-path
```

### Publish

```shell
dotnet nuget push .\nupkg\Static.Voyager.1.0.0.nupkg -k $$API_KEY$$ -s https://api.nuget.org/v3/index.json
```
