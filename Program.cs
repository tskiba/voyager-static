﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Static.Voyager
{
    class Program
    {
        static async Task Main(string[] args)
        {
            if (args.Length == 0)
            {
                var versionString = Assembly.GetEntryAssembly()
                    .GetCustomAttribute<AssemblyInformationalVersionAttribute>()
                    .InformationalVersion
                    .ToString();

                Console.WriteLine($"Static Voyager Generator v{versionString}");
                Console.WriteLine("-------------");
                Console.WriteLine("\nUsage:");
                Console.WriteLine("  graph-to-voyager <file/path.graphql>");
                return;
            }

            var tmpPath = Path.GetTempPath() + "voyager-static/";  
            using var npmRestore = new NodeScript();
            await npmRestore.RunAsync(Console.WriteLine, tmpPath);

            using var script = new NodeScript(args[0]);
            await script.RunAsync(Console.WriteLine, tmpPath);
            
            Console.WriteLine("Generated");
        }
    }
}
