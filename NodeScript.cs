using System;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

public class NodeScript : IDisposable
{
    private static readonly Regex urls = new Regex("(ht|f)tp(s?)\\:\\/\\/[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\'\\/\\\\\\+&%\\$#_]*)?", RegexOptions.IgnoreCase | RegexOptions.Compiled);

    private Process process;
    public string Url { get; private set; }
    public bool HasServer => !string.IsNullOrEmpty(Url);

    public int ProcessId => process?.Id ?? 0;

    private readonly TaskCompletionSource<bool> signal = new TaskCompletionSource<bool>(false);
    private readonly string _Path;

    public NodeScript(string path = null) => _Path = path; 

    public string ReadResource(string name) {
      var assembly = Assembly.GetExecutingAssembly();
      var resourceName = $"Static.Voyager.{name}";

      using var stream = assembly.GetManifestResourceStream(resourceName);
      using var reader = new StreamReader(stream);
      return reader.ReadToEnd();
    }

    private void WriteFileToTempDir(string filename, string tmpPath) {
      var contents = ReadResource(filename);
      File.WriteAllText($"{tmpPath}{Path.DirectorySeparatorChar}{filename}", contents);
    }

    public async Task RunAsync(Action<string> output = null, string tmpPath = "", int timeout = 2000)
    {
      lock(signal) 
      {
          if (_Path is null) {
            output(tmpPath);
            Directory.CreateDirectory(tmpPath);

            WriteFileToTempDir("index.js", tmpPath);
            WriteFileToTempDir("package.json", tmpPath);
            WriteFileToTempDir("npm-shrinkwrap.json", tmpPath);
          }
          if (process == null)
          {
            var current = Directory.GetCurrentDirectory();
            var info = new ProcessStartInfo(_Path is null ? "npm" : "node")
            {
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                Arguments = _Path is null ? "install" : $".{Path.DirectorySeparatorChar}index.js --path {current}{Path.DirectorySeparatorChar}{_Path} --current {current}",
                UseShellExecute = false,
                WorkingDirectory = tmpPath
            };
              
            process = Process.Start(info);
            process.EnableRaisingEvents = true;
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            process.OutputDataReceived += (sender, eventArgs) =>
            {
              output?.Invoke(eventArgs.Data);
              if (!string.IsNullOrEmpty(eventArgs.Data) && string.IsNullOrEmpty(Url))
              {
                var results = urls.Matches(eventArgs.Data);
                if (results.Any())
                 {
                     Url = results.First().Value;
                     signal.SetResult(true);
                 }
              }
            };

              process.ErrorDataReceived += (sender, args) =>
              {
                output?.Invoke(args.Data);
              };

              var cancellationTokenSource = new CancellationTokenSource(timeout);
              cancellationTokenSource.Token.Register(() =>
              {
                if (signal.Task.IsCompleted) return;
                Url = string.Empty;
                signal.SetResult(true);
              }, false);
          }
      } 
      await signal.Task;
    }

    public void Dispose()
    {
      process?.Dispose();
    }
}