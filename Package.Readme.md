# Static Voyager

This is a dotnet tool that takes a single argument that is a relative path to a .graphql schema.
Internally the graphql schema is converted to an introspection json format and compiled into a voyager template.
This file is then written into a new folder called __docs__ in the working directory.
Voyager.html can then be viewed in the browser.
This tool was designed to be run in ci and requires node.

### Using as a local tool

`This tool requires node to work`

If you do not yet have a manifest create on in the root directory of your project

```shell
dotnet new tool-manifest
```

Install the tool

```shell
dotnet tool install Static.Voyager
```

Run it

```shell
dotnet tool run graph-to-voyager -- ./path/to/schema.graphql
```
