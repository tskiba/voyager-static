const fs = require('fs');
const path = require('path');
const argv = require('yargs').argv;
const graphql = require('graphql');

const schema = fs.readFileSync(argv.path, 'utf8');

const sdl = graphql.buildSchema(schema);
const jsonSchema = graphql.introspectionFromSchema(sdl, { descriptions: true });

const voyager = `
<!DOCTYPE html>
<html>
  <head>
    <script src="https://cdn.jsdelivr.net/npm/react@16/umd/react.production.min.js"></script> 
    <script src="https://cdn.jsdelivr.net/npm/react-dom@16/umd/react-dom.production.min.js"></script> 
 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/graphql-voyager/dist/voyager.css" />
    <script src="https://cdn.jsdelivr.net/npm/graphql-voyager/dist/voyager.min.js"></script> 
    <script>


    </script>
  </head>
  <body>
    <div id="voyager">Loading...</div>
    <script>
      function introspectionProvider(introspectionQuery) {
        var schema = ${JSON.stringify( jsonSchema )};
        return new Promise(resolve => resolve(schema));
      }
 
      GraphQLVoyager.init(document.getElementById('voyager'), {
        introspection: introspectionProvider
      })
    </script> 
  </body>
</html>`;

const docsPath = path.join(argv.current, '__docs__');
if (!fs.existsSync(docsPath)) fs.mkdirSync(docsPath);
fs.writeFileSync(path.join(argv.current, '__docs__', 'voyager.html'), voyager, 'utf8');
